const kafka = require('kafka-node');
const express = require('express');

const app = express();
const PORT = process.env.NODE_PORT;
const HOST = process.env.NODE_HOST;
const KAFKA_HOST = 'kafka';
const KAFKA_PORT = 9093;

const Producer = kafka.Producer;
const Consumer = kafka.Consumer;
const KeyedMessage = kafka.KeyedMessage;
const client = new kafka.KafkaClient({ kafkaHost: process.env.KAFKA_ENDPOINT })


// producer
const producer = new Producer(client);

const payloads = [
  { topic: 'topic1', messages: JSON.stringify({ prop1: 'test', prop2: { prop3: 5 }}), partitions: 0 },
  { topic: 'topic2', messages: ['hello', 'world'] }
];

producer.on('ready', function () {
  producer.send(payloads, function (err, data) {
    console.log('send', data, err);
  });
});

producer.on('error', function (err) {
  console.log('producer error', err);
})


// consumer
const consumer = new Consumer(
  client,
  [
    { topic: 'topic1', partitions: 0 }, { topic: 'topic2', partitions: 1 }
  ],
  { autoCommit: true }
);

consumer.on('message', function (message) {
  if (isJson(message.value)) {
    console.log('consumer json', JSON.parse(message.value));
  } else {
    console.log('consumer message', message.value);
  }
});

consumer.on('error', function (err) {
  console.log('consumer error', err);
})





app.get('/', (req, res) => {
  res.send('Kafka worker');
});

const isJson = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
console.log('Kafka Host', process.env.KAFKA_ENDPOINT);